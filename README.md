%*********************************Generalized Moving Peaks Benchmark (GMPB)***********************
%Author: Danial Yazdani
%Last Edited: January 15, 2020
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% ------------
% Description:
% ------------
% 
%  This is the source code of the Generalized Moving Peaks Benchmark (GMPB),
%  which is proposed in the aforementioned reference. This code includes
%  eight scenarios which are explained in Tables II & III of the reference.
% 
%  A simple single-swarm Particle Swarm Optimization (PSO) is used as the optimizer. 
%  In this method, to address the diversity loss issue, a predefined percentage of 
%  the best particles are kept after each environmental change, while the rest are
%  randomized across the search space. In this code, it is assumed that the algorithm 
%  is informed about the environmental changes and it does not need to detect them. 
%% -------
% Inputs (benchmark):
% -------
% 
%    ScenarioNumber : Scenario number. Eight different scenarios have been 
%                     defined in the code whose parameter settings and 
%                     characteristics are defined in Tables II & III of refrence. 
% 
%    RunNumber      : Number of runs (recommended to run at least 31 times).
%
%
% --------
% Outputs:
% --------
%    E_bbc  : It contains the median, mean, and standard error of the average
%             of errors of the best found position before all envioronmental 
%             changes (best before change). Note that since Benchmark.Budget
%             is set to 4999 (Benchmark.ChangeFrequency -1), this is E_bbc. 
%             In case that the researcher whants to apply the Quick Recovery,
%             Benchmark.Budget can be set to smaller numbers. This number 
%             indicate the time after each environmental change where the 
%             algorithm must choose a solution for deployment/implementation. 
% 
% 
%    E_o:     It contains the median, mean, and standard error of the offline
%             error which is the average of the erros of the best solution
%             over all fitness evaluations.
%
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail dot com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2020 Danial Yazdani